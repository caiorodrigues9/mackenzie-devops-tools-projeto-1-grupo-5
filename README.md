# MACKENZIE: GIT - GROUP X #
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Header Authorization ##
Authorization = Basic YWRtaW46YWRtaW4=


## Abstract ##
A aplicação possui as seguintes rotas:
POST /arquivos: substitui a base a ser consultada
GET /carros/fabricantes: retorna uma lista com a média de valores dos carros por fabricante
GET /carros/fabricante/{id_fabricante}: Retoran o valor médio dos carros do fabricante informado
GET /carros/cidades: retorna uma lista com a média de valores dos carros por cidade
GET /carros/cidade/{id_city}: Retoran o valor médio dos carros da cidade informado

## I. Development Requirements ##

### A. Versions ###
 - TODO: ADICIONAR A VERSÃO DO PYTHON QUE DEVE SER UTILIZADA

### B. Running Server ###
```bash
$ pip install -r requirements.txt
$ python .
```

### C. Running Tests ###
```bash
$ pip install -r requirements.txt
$ pytest
```

### D. Command to build project in docker ###
```bash
sudo docker build -t mackenzie-group-x .
sudo docker run -p 5001:5001 -d mackenzie-group-x
```
_______________________________________________________
