import csv

def get():
    # TODO: IMPLEMENTE AQUI
    
    response = {
        "data": "VALOR RETORNADO PELA CONTROLLER QUANDO A SOLICITAÇÃO FOR GET"
    }

    return response

def post(file,app):
    # TODO: IMPLEMENTE AQUI
    if not file:
        response = {
            "data": "Erro: Arquivo não especificado!"
        }
        return [response,400]

    file.save("{}/{}".format(app.config['UPLOAD_FOLDER'], 'dataset.csv'))
    response = {
        "data": "Arquivo {} enviado com sucesso".format('dataset.csv')
    }

    return response, 200

def leArquivo(app):
    arquivo = open("{}/{}".format(app.config['UPLOAD_FOLDER'], 'dataset.csv'))

    carros = csv.DictReader(arquivo)

    return carros
    
