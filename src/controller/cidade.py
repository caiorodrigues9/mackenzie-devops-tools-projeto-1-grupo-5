from audioop import avg
import src.controller.arquivos as arquivos

def getUm(app):
    carros = arquivos.leArquivo(app)

    cidades = {}
    for carro in carros:
        if cidades.get(carro["city"]):
            cidades[carro["city"]] = {"city":carro["city"], "car_value":((int(carro["car_value"]) + int(cidades[carro["city"]]["car_value"]))/2)}
        else:
            cidades[carro["city"]] = {"city":carro["city"], "car_value":int(carro["car_value"])}

    return cidades

def getTodos(app, id_city):
    carros = arquivos.leArquivo(app)

    id_city = id_city.upper()

    cidades = {}
    for carro in carros:
        if carro["city"] == id_city:
            if cidades.get(carro["city"]):
                cidades[carro["city"]] = {"city":carro["city"], "car_value":((int(carro["car_value"]) + int(cidades[carro["city"]]["car_value"]))/2)}
            else:
                cidades[carro["city"]] = {"city":carro["city"], "car_value":int(carro["car_value"])}
        

    return cidades
