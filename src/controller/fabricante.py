from audioop import avg
import src.controller.arquivos as arquivos

def getUm(app):
    carros = arquivos.leArquivo(app)

    fabricantes = {}
    for carro in carros:
        if fabricantes.get(carro["car_make"]):
            fabricantes[carro["car_make"]] = {"car_make":carro["car_make"], "car_value":((int(carro["car_value"]) + int(fabricantes[carro["car_make"]]["car_value"]))/2)}
        else:
            fabricantes[carro["car_make"]] = {"car_make":carro["car_make"], "car_value":int(carro["car_value"])}

    return fabricantes

def getTodos(app, id_car_make):
    carros = arquivos.leArquivo(app)

    id_car_make = id_car_make.upper()

    fabricantes = {}
    for carro in carros:
        if carro["car_make"] == id_car_make:
            if fabricantes.get(carro["car_make"]):
                fabricantes[carro["car_make"]] = {"car_make":carro["car_make"], "car_value":((int(carro["car_value"]) + int(fabricantes[carro["car_make"]]["car_value"]))/2)}
            else:
                fabricantes[carro["car_make"]] = {"car_make":carro["car_make"], "car_value":int(carro["car_value"])}
        

    return fabricantes
