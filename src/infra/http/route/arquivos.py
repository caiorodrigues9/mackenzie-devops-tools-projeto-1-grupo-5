from flask import jsonify, request
from src.infra.utility.http import Http
import src.infra.http.auth as auth
import src.controller.arquivos as arquivos
import flask


def route(app: flask.app.Flask):
    @app.route('/arquivos', methods=['POST'])
    @auth.requires_auth
    def upload_file():
        try:
            
            response = arquivos.post(request.files['file'], app)

            return jsonify(response[0]), response[1]

        except Exception as err:
            response = Http.handle_generic_http_error(err)

            return jsonify(response), 500

    '''@app.route('/example', methods=['POST'])
    @auth.requires_auth
    def request_post_example():
        try:
            response = example.post()

            return jsonify(response), 200
        except Exception as err:
            response = Http.handle_generic_http_error(err)

            return jsonify(response), 500'''
