from wsgiref.util import request_uri
from flask import jsonify, request
from src.infra.utility.http import Http
import src.infra.http.auth as auth
import src.controller.cidade as cidade
import flask


def route(app: flask.app.Flask):
    @app.route('/carros/cidades', methods=['GET'])
    @auth.requires_auth
    def valorMedioTodosPorCidade():
        try:
            
            response = cidade.getUm(app)

            return jsonify(response)

        except Exception as err:
            response = Http.handle_generic_http_error(err)

            return jsonify(response), 500
            
    @app.route("/carros/cidade/<string:id_city>", methods=['GET'])
    @auth.requires_auth
    def valorMedioPorCidade(id_city):
        try:
            
            response = cidade.getTodos(app, id_city)

            return jsonify(response)

        except Exception as err:
            response = Http.handle_generic_http_error(err)

            return jsonify(response), 500