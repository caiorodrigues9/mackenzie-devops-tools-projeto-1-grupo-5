from wsgiref.util import request_uri
from flask import jsonify, request
from src.infra.utility.http import Http
import src.infra.http.auth as auth
import src.controller.fabricante as fabricante
import flask


def route(app: flask.app.Flask):
    @app.route('/carros/fabricantes', methods=['GET'])
    @auth.requires_auth
    def valorMedioTodosPorFabricante():
        try:
            
            response = fabricante.getUm(app)

            return jsonify(response)

        except Exception as err:
            response = Http.handle_generic_http_error(err)

            return jsonify(response), 500
            
    @app.route("/carros/fabricante/<string:id_car_make>", methods=['GET'])
    @auth.requires_auth
    def valorMedioPorFabricante(id_car_make):
        try:
            
            response = fabricante.getTodos(app, id_car_make)

            return jsonify(response)

        except Exception as err:
            response = Http.handle_generic_http_error(err)

            return jsonify(response), 500